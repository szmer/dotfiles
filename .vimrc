" for vim 8
"if (has("termguicolors"))
" set termguicolors
"endif

set nocompatible " don't pretend to be vi
syntax enable             " Turn on syntax highlighting

"set autochdir " autochange dir to current file parent
set ls=2 " always show filename

" Finding files
filetype plugin on
set wildmenu " show matching files on tab completion

"silent! colorscheme eldar
" more contrasting search term highlight
hi Search guibg=white guifg=black cterm=NONE ctermfg=black ctermbg=white
" force background transparency:
hi! Normal ctermbg=NONE guibg=NONE
hi! NonText ctermbg=NONE guibg=NONE

set expandtab " never use tabs (in autoindent)
set shiftwidth=3
set softtabstop=3

set autoindent
filetype plugin indent on

" use mouse in normal mode
"""set mouse=n
" incremental search
set hlsearch
set incsearch

set cursorline " always highlight current line
hi CursorLine term=NONE cterm=NONE guibg=gray25
set signcolumn=yes " an empty, transparent signcolumn - for ALE
highlight SignColumn ctermbg=NONE guibg=NONE

" ALE
let g:ale_completion_enabled = 0
let g:ale_change_sign_column_color = 1
let g:ale_echo_cursor = 1
let g:ale_echo_delay = 500
let g:ale_lint_delay = 50
let g:ale_linters = { 'python': ['pyflakes'] }
" highlight error signcolumn with something less intrusive (Eldar NonText
" color: 'EldarBlue')
highlight ALESignColumnWithErrors ctermbg=blue guibg=#729FCF

" Vimtex.
let g:tex_flavor="latex"
let g:vimtex_quickfix_mode=0
set conceallevel=2
let g:tex_conceal="abdmg"
highlight Conceal guibg=gray20 guifg=White

" Vim-orgmode.
highlight Folded guibg=NONE

" Slimv.
"let g:slimv_lisp = '/home/szymonrutkowski/lisp.sh'

" Load all plugins now.
" Plugins need to be added to runtimepath before helptags can be generated.
packloadall
" Load all of the helptags now, after plugins have been loaded.
" All messages and errors will be ignored.
silent! helptags ALL

set tw=0 " disable orgmode autohardwrap on 78 chars
set colorcolumn=100
